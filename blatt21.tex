%
\documentclass[
  headsepline,
  headings=standardclasses
]{scrartcl}
%
\usepackage[margin=100pt]{geometry}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{dsfont}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{tikz-cd}
%
%
\newlength{\aufgabenskip}
\setlength{\aufgabenskip}{1.5em}
\newcounter{aufgabennummer}
\newenvironment{aufgabe}[1][]{
  \addtocounter{aufgabennummer}{1}
  \textbf{Aufgabe \theaufgabennummer{}.} \emph{#1} \par
}{\vspace{\aufgabenskip}}

\newenvironment{bonusaufgabe}[1][]{
  \textbf{Bonusufgabe} \emph{#1} \par
}{\vspace{\aufgabenskip}}

\setlength\parskip{\medskipamount}
\setlength\parindent{0pt}

\renewcommand*\theenumi{\alph{enumi}}
\renewcommand*\theenumii{\arabic{enumii}}
\renewcommand{\labelenumi}{\theenumi)}
\renewcommand{\labelenumii}{\theenumii.}

%
%
\newcommand{\A}{\mathcal{A}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\D}{\mathcal{D}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\G}{\mathcal{G}}
\renewcommand{\H}{\mathcal{H}}
\newcommand{\HOM}{\mathcal{H}om}
\newcommand{\K}{\mathcal{K}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\N}{\mathcal{N}}
\renewcommand{\O}{\mathcal{O}}
\renewcommand{\P}{\mathcal{P}}
\newcommand{\R}{\mathcal{R}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\T}{\mathcal{T}}
\renewcommand{\AA}{\ensuremath{\mathbb{A}}}
\newcommand{\QQ}{\ensuremath{\mathbb{Q}}}
\newcommand{\ZZ}{\ensuremath{\mathbb{Z}}}
\newcommand{\RR}{\ensuremath{\mathbb{R}}}
\newcommand{\FF}{\ensuremath{\mathbb{F}}}
\newcommand{\CC}{\ensuremath{\mathbb{C}}}
\newcommand{\NN}{\ensuremath{\mathbb{N}}}
\newcommand{\PP}{\ensuremath{\mathbb{P}}}
\newcommand{\FFF}{\ensuremath{\mathfrak{F}}}
\newcommand{\UUU}{\ensuremath{\mathfrak{U}}}
\newcommand{\coloneqq}{\vcentcolon=}
\newcommand{\smallplaceholder}{\underline{\ \ }}
\newcommand{\Mod}{\mathrm{Mod}}
\newcommand{\Ab}{\mathrm{Ab}}
\newcommand{\Bigwedge}{\Lambda}
\newcommand{\SPEC}{\ensuremath{\mathbf{Spec}}}
\newcommand{\zmatrix}[4]{\ensuremath{\left(\begin{matrix} #1 & #2\\ #3 & #4\end{matrix}\right)}}
\newcommand{\hinw}{\noindent\emph{\textbf{Hinweis:}}\quad}
\newcommand{\cH}{\check{H}}
\newcommand{\cC}{\check{C}}
%
\newtheorem*{theorem}{Theorem}
\theoremstyle{remark}
\newtheorem*{bem}{Bemerkung}
\newtheorem*{lsg}{Lösungsvorschlag}
\newtheorem*{hin}{\textbf{Hinweis}}
\newtheorem*{anl}{\textbf{Anleitung}}
%
%
%
\usepackage[framemethod=tikz]{mdframed}
\surroundwithmdframed[linecolor=black, backgroundcolor=black!10]{aufg}
%
\DeclareMathOperator{\Cl}{Cl}
\DeclareMathOperator{\Pic}{Pic}
\DeclareMathOperator{\Div}{Div}
\DeclareMathOperator{\PSh}{PSh}
\DeclareMathOperator{\Sh}{Sh}
\DeclareMathOperator{\Spec}{Spec}
\DeclareMathOperator{\Proj}{Proj}
\DeclareMathOperator{\QCoh}{QCoh}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\colim}{colim}
%\DeclareMathOperator{\Mod}{Mod}
%
\usepackage{scrlayer-scrpage}
\lohead{Schematheorie II, SoSe 2017}
\rohead{Prof. Maxim Smirnov\\ Lukas Stoll}
\cofoot{}
\pagestyle{scrheadings}
\begin{document}
%
%
%
\section*{Übung 21}
Auf dem gesamten Blatt sei $C$ eine reguläre projektive Kurve über einem Körper $k$.

\begin{aufgabe}[Additivität der Eulercharakteristik]
  Sei $(X,d)$ ein Komplex von Vektorräumen, sodass $H^p(X)$ endlichdimensional ist und für fast alle $p$ verschwindet.
  Die Eulercharakteristik $\chi(X)$ von $X$ ist definiert als
  \[\chi(X)=\sum_{p\in\ZZ}(-1)^p\dim H^p(X).\]
  Beweise:
  \begin{enumerate}
  \item Ist $0\to X\to Y\to Z\to 0$ eine kurze exakte Sequenz von Komplexen, so ist $\chi(Y)=\chi(X)+\chi(Z)$.
  \item Ist $0\to\F\to\G\to\H\to 0$ eine kurze exakte Sequenz kohärenter Garben auf einem projektiven $k$-Schema $X$, so
    ist $\chi(X,\G)=\chi(X,\F)+\chi(X,\H)$.
  \end{enumerate}  
\end{aufgabe}

\begin{aufgabe}[Riemann-Roch für reguläre projektive Kurven]
  Sei $D=\sum a_p p$ ein Divisor auf $C$.
  Zeige
  \[\chi(C,\O(D))=\deg D+\chi(C,\O).\]

  {\tiny Tipp: Betrachte zuerst den Fall $D=0$. Zeige anschließend, dass die Gleichung für einen Divisor $D$ genau dann erfüllt ist,
    wenn sie für $D+p$ erfüllt ist, wobei $p$ ein Punkt auf $C$ ist.
    Verwende dazu die kurze exakte Sequenz $0\to\O(-p)\to\O\to i_*\O_{\Spec(k(p))}\to 0$, welche von der abgeschlossen Inklusion $\Spec(k(p))\hookrightarrow C$ stammt.}
\end{aufgabe}

\begin{aufgabe}
  Sei $D$ ein Divisor auf $C$.
  Finde eine Bijektion zwischen $\left(\Gamma(X,\O(D))\setminus\lbrace 0\rbrace\right)/k^*$ und
  der Menge der effektiven Divisoren auf $X$, welche zu $D$ linear äquivalent sind.
\end{aufgabe}

\begin{aufgabe}
  Sei $C$ vom Geschlecht $0$.
  Zeige, dass je zwei Punkte vom gleichen Grad auf $C$ linear äquivalent sind.

  {\tiny Tipp: Riemann Roch und vorherige Aufgabe.}
\end{aufgabe}

\begin{aufgabe}
  Sei $P$ ein $k$-rationaler Punkt auf $C$.
  Finde alle möglichen Werte von $h^0(X,\O(P))$.
\end{aufgabe}



\bibliographystyle{plain}
\bibliography{bib.bib}
%
%
\end{document}
